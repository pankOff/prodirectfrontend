import { createAction } from 'redux-act';
import React from "react";

// список кампаний
export function fetchCampaings () {
    return (dispatch) => {

        fetch(window.backEndPoint+'/getCampaigns', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem("direct-auth-token")
            }
        })
            .then((res) => res.json())
            .then((data) => {
                dispatch(putCampaings(data.result.Campaigns));
            });
    }
}
export const putCampaings = createAction('put campaings');
export const putYandexUsers = createAction('put yandex users');
export const putAdGroups = createAction('put ad groups');

export const selectCampaign = createAction('select campaign');
export const selectAllCampaigns = createAction('select campaign');



// список стратегий
export function fetchStrategies () {
    return (dispatch, getState) => {
        if (getState().strategies.length > 0) {
            // фетчим стратегии один раз
            return;
        }
        /*fetch('/mocks/getStrategy.json')
            .then((res) => res.json())
            .then((data) => {
                dispatch(putStrategies(data || []));
            });*/

        fetch(window.backEndPoint+'/getStrategies',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem("direct-auth-token")
            }
        })
            .then((res) => res.json())
            .then((response) => {
                if (!response.Status) {
                    console.log("ERROR")
                } else {
                    console.log("response ", response);
                    dispatch(putStrategies(response.strategies || []));
                }
            })
        // TODO catch errors
    }
}

//список яндекс юзеров
export function fetchUsersAndCampaigns() {
    return (dispatch) => {
        fetch(window.backEndPoint+'/getCampaignsAndUsers', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem("direct-auth-token")
            }
        })
            .then((res) => res.json())
            .then((response) => {
                if (!response.Status) {
                    console.log("ERROR")
                } else {
                    dispatch(putYandexUsers(response.users));
                }
            });
    }
}

export function fetchUsers(selectedCampaign, selectedUser) {
    return (dispatch) => {
        let formDate = {
            yandexUserEmail: selectedUser,
            campaignsIds : [selectedCampaign]
        };

        fetch(window.backEndPoint+'/getAdGroupsForCampaigns', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem("direct-auth-token")
            },
            body: JSON.stringify(formDate)
        })
            .then((res) => res.json())
            .then((response) => {
                if (!response.Status) {
                    console.log("ERROR")
                } else {
                    dispatch(putAdGroups(response));
                }
            });
    }
}
export const putStrategies = createAction('put strategies');



export const putJivoLoaded = createAction('jivosite loaded');

