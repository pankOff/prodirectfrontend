import { DEFAULT_STATE } from "./DEFAULT_STATE";
import { createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from "./rootReducer";


const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
//window.frontEndPoint = 'http://localhost:3000/#';
window.frontEndPoint = 'https://directpro.tk/#';
//window.backEndPoint = 'http://localhost:9034';
window.backEndPoint = 'https://directpro.tk:9034';

export const store = createStoreWithMiddleware(rootReducer);
