export const DEFAULT_STATE = {
    campaigns: [],
    campaignsPending: true,
    usersPending: true,
    adGroupsPending: true,
    users: [],
    adGroups: [],
    strategies: [],
};
