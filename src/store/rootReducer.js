import { createReducer } from 'redux-act';
import { DEFAULT_STATE } from "./DEFAULT_STATE";
import {
    putCampaings,
    putJivoLoaded,
    putStrategies,
    putYandexUsers,
    selectAllCampaigns,
    selectCampaign,
    putAdGroups
} from "./actions";

export const rootReducer = createReducer({
    // список кампаний
    [putCampaings]: (state, campaigns) => ({
        ...state,
        campaigns,
        campaignsPending: false,
    }),
    // список яндекс юзеров
    [putYandexUsers]: (state, users) => ({
        ...state,
        users,
        usersPending: false,
    }),
    [putAdGroups]: (state, adGroupsAdsKeywords) => ({
        ...state,
        adGroupsAdsKeywords,
        adGroupsPending: false,
    }),
    [selectCampaign]: (state, { campaignId, selected }) => ({
        ...state,
        campaigns: state.campaigns.map((campaign) => {
            return campaign.Id === campaignId
                ? { ...campaign, selected }
                : campaign
        }),
    }),
    [selectAllCampaigns]: (state, { selected }) => ({
        ...state,
        campaigns: state.campaigns.map(
            (campaign) => ({ ...campaign, selected })
        ),
    }),


    [putStrategies]: (state, strategies) => ({
        ...state,
        strategies,
    }),



    [putJivoLoaded]: (state) => ({
        ...state,
        jivoLoaded: true,
    })

}, DEFAULT_STATE);
