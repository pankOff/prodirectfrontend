/**
 * See https://github.com/kulakowka/mui-datatables/blob/master/src/textLabels.js
 */
module.exports = {
    body: {
        noMatch: "Извините, записей не найдено",
        toolTip: "Сортировка",
    },
    pagination: {
        next: "Следующая страница",
        previous: "Предыдущая страница",
        rowsPerPage: "Строк на страницу",
        displayRows: "из",
    },
    toolbar: {
        search: "Поиск",
        downloadCsv: "Загрузить CSV",
        print: "Печать",
        viewColumns: "Показать колонки",
        filterTable: "Фильтр таблицы",
    },
    filter: {
        all: "Все",
        title: "ФИЛЬТРЫ",
        reset: "СБРОСИТЬ",
    },
    viewColumns: {
        title: "Показать колонки",
        titleAria: "Показать/скрыть колонки таблицы",
    },
    selectedRows: {
        text: "выделено",
        delete: "Удалить",
        deleteAria: "Удалить выбранные строки",
    },
};
