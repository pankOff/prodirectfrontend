import React, { Component } from 'react';
import { NavBarConntected } from './components/NavBar';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { Route } from 'react-router-dom';
import { StrategySettingsConnected } from "./components/StrategySettings";
import { CampaignsListConnected } from './components/CampaignsList';
import LogInForm from "./components/login/LogInForm";
import SignUpForm from "./components/login/SignUpForm";
import AddYandexUserForm from "./components/login/AddYandexUserForm";
import { RootElementConnected } from "./components/RootElement"
import NewStrategy from "./components/NewStrategy";
import {StrategiesTestConnected} from "./components/Strategies";


const Campaign = (props) => {
    return <React.Fragment>
    </React.Fragment>;
};

const Campaigns = () => {
    if (localStorage.getItem("direct-auth-token")) {

        return <React.Fragment>
            <Grid container style={{ padding: 20 }}>
                <Grid item xs={12}>
                    <Paper>
                        <StrategySettingsConnected/>
                    </Paper>
                </Grid>
            </Grid>

            <CampaignsListConnected/>
        </React.Fragment>;

    } else {
        window.location.assign(window.frontEndPoint+'/login/');
        return <LogInForm/>;
    }

};

class App extends Component {
    render() {
        return  <React.Fragment>
            <NavBarConntected/>
            <Route exact path="/login" component={LogInForm}/>
            <Route exact path="/" component={RootElementConnected}/>
            <Route exact path="/campaign" component={Campaign}/>
            <Route exact path="/signup" component={SignUpForm}/>
            <Route exact path="/add_yandex_user" component={AddYandexUserForm}/>

            <Route exact path="/new_strategy" component={NewStrategy}/>
            <Route exact path="/strategies" component={StrategiesTestConnected}/>
            {/*<Route path="/campaigns" component={Campaigns}/>*/}
        </React.Fragment>
    }
}

export default App;
