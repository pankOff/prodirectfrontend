import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Typography from "@material-ui/core/Typography";

const columns = [
    { id: 'name', label: ''},
    { id: 'bid', label: 'Активная Ставка'},
    { id: 'price', label: 'Списываемая цена'},
];

function createData(name, bid, price) {
    return { name, bid, price };
}

const rows = [
    createData('Максимальная ставка', 'Макс.Ставка', ''),
    createData('1-е спецразмещение', '1С', '1СЦ'),
    createData('2-е спецразмещение', '2С', '2СЦ'),
    createData('3-е спецразмещение', '3С', '3СЦ'),
    createData('4-е спецразмещение', '4С', '4СЦ'),
    createData('1-е место', '1М', '1МЦ'),
    createData('2-е место', '2М', '2МЦ'),
    createData('3-е место', '3М', '3МЦ'),
    createData('4-е место', '4М', '4МЦ')
];

const useStyles = makeStyles({
    root: {
    },
    container: {
        maxHeight: 500,
    },
});

export default function VariablesTable() {
    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    return (
        <Paper className={classes.root}>
            <Typography variant="h6" id="tableTitle" component="div">
                Переменные
            </Typography>
            <TableContainer className={classes.container}>
                <Table stickyHeader
                       aria-label="sticky table"
                       size="small"
                >
                    <TableHead>
                        <TableRow>
                            {columns.map((column) => (
                                <TableCell
                                    key={column.id}
                                    align={column.align}
                                    style={{ minWidth: column.minWidth }}
                                >
                                    {column.label}
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
                            return (
                                <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                                    {columns.map((column) => {
                                        const value = row[column.id];
                                        return (
                                            <TableCell key={column.id} align={column.align}>
                                                {column.format && typeof value === 'number' ? column.format(value) : value}
                                            </TableCell>
                                        );
                                    })}
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[5, 10, 100]}
                component="div"
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />
        </Paper>
    );
}
