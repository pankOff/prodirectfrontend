import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import ImageIcon from '@material-ui/icons/Image';
import ListSubheader from '@material-ui/core/ListSubheader';
import Checkbox from '@material-ui/core/Checkbox';


class ListItemsTmp extends React.Component{
    constructor(props) {
        super(props);
        this.handleListItemClick = this.handleListItemClick.bind(this);
        this.handleSelectCampaign = this.handleSelectCampaign.bind(this);
        this.state = {selectedItem: ''};
    }

    handleListItemClick (event, index) {
        this.props.onChange(index);
    };

    handleSelectCampaign (event, id) {
        this.props.onClickCheckbox(id);
    }

    renderList () {
        const selectedUser = this.props.selectedUser;
        const selectedCampaign = this.props.selectedCampaign;
        const name = this.props.name;
        const usersList = this.props.users;
        let campaignsList = [];
        usersList.forEach((user)=>{
            if (user.email === selectedUser) {
                campaignsList = user.campaigns;
            }
        })
        if (name === 'Аккаунты') {
            return (
                usersList.map((item) => (
                    <ListItem
                        key={`item-${item.email}`}
                        button
                        selected={selectedUser === item.email}
                        onClick={(event) => this.handleListItemClick(event, item.email)}
                        alignItems="flex-start"
                    >
                        <ListItemText secondary={item.email.substring(0, item.email.indexOf('@'))} />
                    </ListItem>
                ))
            );
        }
        if (name === 'Кампании') {
            return (
                campaignsList.map((campaign) => (
                    <ListItem
                        key={`item-${campaign}`}
                        button
                        selected={selectedCampaign === campaign.Id}
                        onClick={(event) => this.handleListItemClick(event, campaign.Id)}
                        alignItems="flex-start"
                        style={{fontSize: 16, color: "black", margin: 0, padding: 10}}
                    >
                        {/*TODO: enable this checkbox and dependencies between checkboxes of different levels campaigns/group/keyword*/}
                        {/*<Checkbox
                            onChange={(event) => this.handleSelectCampaign(event, campaign.Id)}
                            name="checkedB"
                            color="primary"
                        />*/}
                        <ListItemText secondary={campaign.Name}/>
                        {/*<ListItemText primary={`${campaign.Name}`} secondary={`${campaign.Id}`} />*/}
                    </ListItem>
                ))
            );
        }
    }

    render() {

        return (

                <List
                    component="nav"
                    aria-label="main mailbox folders"
                    subheader={
                        <ListSubheader component="div" id="nested-list-subheader">
                            {this.props.name}
                        </ListSubheader>
                    }
                >
                    {this.renderList()}

                </List>
        );
    }


}

export default ListItemsTmp;