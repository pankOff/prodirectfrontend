import React from "react";
import {fetchUsers, fetchUsersAndCampaigns} from "../store/actions";
import {connect} from "react-redux";
import Grid from "@material-ui/core/Grid";
import AdGroupCard from "./AdGroupCard";

class AdGroupField extends React.Component{
    constructor(props) {
        super(props);
        this.state = {selectedCampaign : '', pending: true};
    }

    renderGroup() {
        if (!this.props.pending) {
            return (
                <React.Fragment>
                    <Grid item xs={9}>
                        {this.props.adGroupsAdsKeywords.AdGroups.map((adGroup)=>(
                            <AdGroupCard
                                adGroupsAdsKeywords={this.props.adGroupsAdsKeywords}
                                adGroup={adGroup}
                                onClickCheckbox={this.props.onClickCheckbox}
                                onClickKeywordCheckbox={this.props.onClickKeywordCheckbox}
                                selectedUser={this.props.selectedUser}
                                selectedKeywords={this.props.selectedKeywords}
                            />
                        ))}
                    </Grid>

                </React.Fragment>
            );
        }

        return (
            <React.Fragment>
            </React.Fragment>
        );

    }
    render() {
        const { adGroupsAdsKeywords = [], pending = true} = this.props;

        if (this.state.selectedCampaign != this.props.selectedCampaign) {
            this.setState({selectedCampaign : this.props.selectedCampaign})
            this.props.fetchUsers(this.props.selectedCampaign, this.props.selectedUser);
        }
        return (
            this.renderGroup()
        );
    }

    componentDidMount() {
        this.props.fetchUsers(this.props.selectedCampaign, this.props.selectedUser);
    }
}

const mapStateToProps = ({ adGroupsAdsKeywords, adGroupsPending }) => ({
    adGroupsAdsKeywords,
    pending: adGroupsPending,
});

const mapDispatchToProps = (dispatch) => ({
    fetchUsers: (selectedCampaign, selectedUser) => dispatch(fetchUsers(selectedCampaign, selectedUser))
});

export const AdGroupFieldConnected = connect(mapStateToProps, mapDispatchToProps)(AdGroupField);