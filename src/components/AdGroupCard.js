import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid";
import Checkbox from "@material-ui/core/Checkbox";
import {StrategySettingsConnected} from "./StrategySettings";


class AdGroupCard extends React.Component{

    constructor(props) {
        super(props);
        this.handleSelectAdGroup = this.handleSelectAdGroup.bind(this);
        this.handleSelectKeyword = this.handleSelectKeyword.bind(this);
        this.state = {selectedKeywords : []};
    }

    handleSelectAdGroup(event, id) {
        this.props.onClickCheckbox(id);
        let selectedKeywords = [];
        this.props.adGroupsAdsKeywords.Keywords
            .filter((ad)=>(ad.AdGroupId === this.props.adGroup.Id))
            .map((keyword)=>{
                    this.handleSelectKeyword(event, keyword.KeywordId);
                    //selectedKeywords.push(keyword.KeywordId);
                }
            )
        this.setState({selectedKeywords : this.props.selectedKeywords})
    }

    handleSelectKeyword(event, id) {
        this.props.onClickKeywordCheckbox(id);
        this.setState({selectedKeywords : this.props.selectedKeywords})
    }

    renderPositions() {
        return <React.Fragment>
            <Typography style={{fontSize: 12}}>
                1 Спец.
            </Typography>
            <Typography style={{fontSize: 12}}>
                2 Спец.
            </Typography>
            <Typography style={{fontSize: 12}}>
                3 Спец.
            </Typography>
            <Typography style={{fontSize: 12}}>
                4 Спец.
            </Typography>
            <Typography style={{fontSize: 12}}>
                1 Место
            </Typography>
            <Typography style={{fontSize: 12}}>
                2 Место
            </Typography>
            <Typography style={{fontSize: 12}}>
                3 Место
            </Typography>
            <Typography style={{fontSize: 12}}>
                4 Место
            </Typography>
        </React.Fragment>
    }
    render() {
        const useStyles = makeStyles({
            root: {
                maxWidth: 275,
            },
            bullet: {
                display: 'inline-block',
                margin: '0 2px',
                transform: 'scale(0.8)',
            },
            title: {
                fontSize: 4,
            },
            regular: {
                fontSize: 4,
            },
            pos: {
                marginBottom: 12,
            },
        });
        const classes = useStyles;
        return (
            <Card className={classes.root} variant="outlined">
                <CardContent>
                    <Typography variant="h6" component="h4">
                        {/*TODO: enable this checkbox and dependencies between checkboxes of different levels campaigns/group/keyword*/}
                        <Checkbox
                            onChange={(event) => this.handleSelectAdGroup(event, this.props.adGroup.Id)}
                            name="adGroupCheckbox"
                            color="primary"
                        />
                        {this.props.adGroup.Name}
                    </Typography>
                    <Grid container spacing={1}>
                        <Grid item xs={3}>
                            {this.props.adGroupsAdsKeywords.Ads
                                .filter((ad)=>(ad.AdGroupId === this.props.adGroup.Id))
                                .map((ad)=>(
                                <Card className={classes.root} variant="outlined">
                                    <CardContent>
                                        <Typography variant="body1" component="p" style={{color: "blue", fontWeight: "bold"}}>
                                            {ad.TextAd.Title}
                                        </Typography>
                                        <Typography className={classes.title} color="textPrimary" gutterBottom>
                                            {ad.TextAd.Text}
                                        </Typography>
                                        <Typography className={classes.title} color="textSecondary" gutterBottom>
                                            {"Статус: " + ad.Status + " " + ad.Id}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            ))}
                        </Grid>
                        <Grid item xs={9}>
                            {this.props.adGroupsAdsKeywords.Keywords
                                .filter((ad)=>(ad.AdGroupId === this.props.adGroup.Id))
                                .map((keyword)=>(
                                    <Grid container spacing={1}>
                                        <Grid item xs={3}>
                                            <Typography className={classes.title} style={{color: "blue"}} gutterBottom>
                                                <Checkbox
                                                    checked={this.state.selectedKeywords.includes(keyword.KeywordId)}
                                                    onChange={(event) => this.handleSelectKeyword(event, keyword.KeywordId)}
                                                    name="keywordCheckbox_Group_"
                                                    id={keyword.KeywordId}
                                                    color="primary"
                                                />
                                                {keyword.Keyword}
                                            </Typography>
                                            <Typography style={{fontSize: 12}} gutterBottom>
                                                {'Ставка: ' + keyword.Bid/1000000}
                                            </Typography>
                                            <Typography style={{fontSize: 12}} gutterBottom>
                                                {'Цена: ' + keyword.Price/1000000}
                                            </Typography>

                                            <Typography style={{ fontSize: 12}}>
                                                Стартегия:
                                            </Typography>
                                            <Typography style={{color: "black", fontSize: 12}}>
                                                {keyword.Strategy}
                                            </Typography>
                                            <Typography style={{fontSize: 12}}>
                                                Максимальная ставка:
                                            </Typography>
                                            <Typography style={{color: "black", fontSize: 12}}>
                                                {keyword.MaxBid/1000000}
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={1}>
                                            <Typography style={{color: "red", fontSize: 12}}>
                                                Позиция:
                                            </Typography>
                                            {this.renderPositions()}
                                         </Grid>
                                        <Grid item xs={1}>
                                            <Typography style={{color: "red", fontSize: 12}}>
                                                Ставка:
                                            </Typography>
                                            {keyword.AuctionBids.map((bid)=>(
                                                <Typography style={{color: "black", fontSize: 12}}>
                                                    {bid.Bid/1000000}
                                                </Typography>
                                            ))}
                                        </Grid>
                                        <Grid item xs={1}>
                                            <Typography style={{color: "red", fontSize: 12}}>
                                                Цена:
                                            </Typography>
                                            {keyword.AuctionBids.map((bid)=>(
                                                <Typography style={{color: "black", fontSize: 12}}>
                                                    {bid.Price/1000000}
                                                </Typography>
                                            ))}
                                        </Grid>
                                        <Grid item xs={6}>
                                            <StrategySettingsConnected
                                                campaignsForStrategy={[]}
                                                adGroupsForStrategy={[]}
                                                keywordsForStrategy={[keyword.KeywordId]}
                                                keyword={keyword.Keyword}
                                                selectedUser={this.props.selectedUser}
                                                isMultiple={false}
                                            />
                                        </Grid>
                                    </Grid>
                            ))}
                        </Grid>
                    </Grid>

                </CardContent>
            </Card>
        );
    }
}

export default AdGroupCard;