import React from "react";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import TablePagination from "@material-ui/core/TablePagination";
import Button from "@material-ui/core/Button";
import SaveIcon from "@material-ui/icons/Save";
import {putCampaings} from "../store/actions";

const columns = [
    { id: 'name', label: 'Название'},
    { id: 'formula', label: 'Формула'},
];

function handleDelete(event, id) {
    let url = window.backEndPoint+'/strategy/delete/'+id
    fetch(url, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem("direct-auth-token")
        }
    })
        .then((res) => res)
        .then((data) => {
            document.location.reload(true);
        });
};

export default function StrategiesTable(props) {
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    return (
        <React.Fragment>
            <Typography variant="h6" id="tableTitle" component="div">
                Стратегии
            </Typography>
            <TableContainer>
                <Table stickyHeader
                       aria-label="sticky table"
                       size="small"
                >
                    <TableHead>
                        <TableRow>
                            {columns.map((column) => (
                                <TableCell
                                    key={column.id}
                                >
                                    {column.label}
                                </TableCell>
                            ))}
                            <TableCell>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {props.strategyList.map((strategy) => {
                            return (
                                <TableRow hover role="checkbox" tabIndex={-1} key={strategy.id}>
                                    <TableCell align="left">
                                        {strategy.name}
                                    </TableCell>
                                    <TableCell align="left">
                                        {strategy.formula}
                                    </TableCell>
                                    <TableCell align="right">
                                        <IconButton onClick={(event) => handleDelete(event, strategy.id)}>
                                            <DeleteIcon/>
                                        </IconButton>
                                        {/*<IconButton aria-label="edit">
                                            <EditIcon />
                                        </IconButton>*/}
                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[5, 10, 100]}
                component="div"
                rowsPerPage={rowsPerPage}
                count={props.strategyList.length}
                page={page}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />
    </React.Fragment>
    )
}