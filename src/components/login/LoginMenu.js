import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InputIcon from '@material-ui/icons/Input';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

const StyledMenu = withStyles({
    paper: {
        border: '1px solid #d3d4d5',
    },
})((props) => (
    <Menu
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
        }}
        {...props}
    />
));

const StyledMenuItem = withStyles((theme) => ({
    /*root: {
        '&:focus': {
            backgroundColor: theme.palette.primary.main,
            '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
                color: theme.palette.common.white,
            },
        },
    },*/
}))(MenuItem);

function signout() {
    localStorage.removeItem("direct-auth-token");
    window.location.assign(window.frontEndPoint+'/login/');
}


export default function LoginMenu() {
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div style={{position: "absolute", right: 20}}>
            <Button
                aria-controls="customized-menu"
                aria-haspopup="true"
                variant="contained"
                style={{background: "#F6E332", color: "black"}}
                onClick={handleClick}

            >
                Профиль
            </Button>
            <StyledMenu
                id="customized-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                {localStorage.getItem("direct-auth-token")
                ?
                <StyledMenuItem onClick={(e) => {signout(e); handleClose()}}>
                    <ListItemIcon>
                        <InputIcon fontSize="small" />
                    </ListItemIcon>
                    <ListItemText primary="Выйти" />
                </StyledMenuItem>
                :
                <StyledMenuItem onClick={(e) => {signout(e); handleClose()}}>
                    <ListItemIcon>
                        <ExitToAppIcon fontSize="small" />
                    </ListItemIcon>
                    <ListItemText primary="Войти" />
                </StyledMenuItem>
                }
            </StyledMenu>
        </div>
    );
}
