import React from "react";
import Paper from "@material-ui/core/Paper";
import TextField from '@material-ui/core/TextField';
import Grid from "@material-ui/core/Grid";
import FormControl from "@material-ui/core/FormControl";
import Button from "@material-ui/core/Button";

class LogInForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {errorMessageLogin: null, errorMessagePassword: null, Status: true};
    }

    signup () {
        window.location.assign(window.frontEndPoint+'/signup/');
    }

    login () {
        let formDate = {
            username: document.getElementById("login").value,
            password: document.getElementById("password").value,
            email: document.getElementById("login").value
        };

        fetch(window.backEndPoint+'/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formDate)
        })
            .then((res) => res.json())
            .then((response) => {
                if (response.Status) {
                    console.log(response);
                    localStorage.setItem("direct-auth-token", response.directAuthToken);
                    window.location.assign(window.frontEndPoint+'/');
                } else {
                    this.setState({Status : false,
                        errorMessageLogin : response.errorMessageLogin,
                        errorMessagePassword : response.errorMessagePassword})
                }
            });

    }

    render() {
        return (
            <React.Fragment>
                <Grid container style={{ padding: 20, marginTop: 80 }}
                      justify="center">
                    <Grid item xs={6}>
                        <Paper>
                            <Grid container
                                  direction="column"
                                  justify="center"
                                  alignItems="center"
                                  xs = {12}>
                                <Grid item xs={10}>
                                    <h2>С возвращением в ProDirect!</h2>
                                </Grid>
                            </Grid>
                            <Grid container
                                  direction="row"
                                  justify="center"
                                  xs={12} style={{padding: 20}}
                                    >
                                <Grid item xs={0} >
                                    <FormControl fullWidth={true}>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={8}>
                                    <FormControl fullWidth={true}>
                                        {
                                            this.state.errorMessageLogin != null
                                                ? <TextField
                                                    id="login"
                                                    label="Логин или Email"
                                                    error
                                                    helperText={this.state.errorMessageLogin}
                                                />
                                                : <TextField id="login" label="Логин или Email"/>
                                        }
                                        {
                                            this.state.errorMessagePassword != null
                                                ? <TextField id="password"
                                                             label="Пароль"
                                                             type="password"
                                                             error
                                                             helperText={this.state.errorMessagePassword}
                                                />
                                                : <TextField id="password" label="Пароль" type="password"/>
                                        }
                                    </FormControl>
                                    <Button variant="contained" color="primary" style={{marginTop: 20, marginRight: 20, background: "#F6E332", color: "black"}} onClick={(e) => this.login(e)}>
                                        Войти
                                    </Button>
                                    <Button variant="outlined" color="primary" style={{marginTop: 20, color: "black"}} onClick={(e) => this.signup(e)}>
                                        Зарегистрироваться
                                    </Button>
                                </Grid>
                            </Grid>

                        </Paper>
                    </Grid>
                </Grid>
            </React.Fragment>
        );
    }

}

export default LogInForm;