import React from "react";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {putCampaings} from "../../store/actions";

class SignUpForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {errorMessageUsername: null, errorMessageEmail: null, Status: true};
    }

    onclick (e) {
        let formDate = {
            username: document.getElementById("username").value,
            password: document.getElementById("password").value,
            email: document.getElementById("email").value
        };

        fetch(window.backEndPoint+'/addNewDirectUser', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formDate)
            })
            .then((res) => res.json())
            .then((response) => {
                console.log("RESPONSE", response);
                if (response.Status) {
                    localStorage.setItem("direct-auth-token", response.directAuthToken);
                    window.location.assign(window.frontEndPoint+'/');
                } else {
                    this.setState({Status : false,
                        errorMessageUsername : response.errorMessageUsername,
                        errorMessageEmail : response.errorMessageEmail,})
                }
            });
    }

    render() {
        return (
            <React.Fragment>
                <Grid container style={{ padding: 20, marginTop: 80 }}
                      justify="center">
                    <Grid item xs={6}>
                        <Paper>
                            <Grid container
                                  direction="column"
                                  justify="center"
                                  alignItems="center"
                                  xs = {12}>
                                <Grid item xs={10}>
                                    <h2>Добро пожаловать в ProDirect!</h2>
                                </Grid>
                            </Grid>
                            <Grid container
                                  direction="row"
                                  justify="center"
                                  xs={12} style={{padding: 20}}
                            >
                                <Grid item xs={0} >
                                    <FormControl fullWidth={true}>
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={8}>
                                    <FormControl fullWidth={true}>
                                        {
                                            this.state.errorMessageEmail != null
                                            ? <TextField
                                                id="email"
                                                label="Email"
                                                type="email"
                                                error
                                                helperText={this.state.errorMessageEmail}
                                            />
                                            : <TextField id="email" label="Email" type="email"/>
                                        }
                                        {
                                            this.state.errorMessageUsername != null
                                            ? <TextField id="username"
                                                         label="Логин"
                                                         error
                                                         helperText={this.state.errorMessageUsername}
                                                />
                                            : <TextField id="username" label="Логин"/>
                                        }
                                        <TextField id="password" label="Пароль" type="password"/>
                                        <TextField id="password2" label="Поторите пароль"  type="password"/>

                                    </FormControl>
                                    <Button variant="contained" color="primary" style={{marginTop: 20, background: "#F6E332", color: "black"}} onClick={(e) => this.onclick(e)}>
                                        Зарегистрироваться
                                    </Button>
                                </Grid>
                            </Grid>

                        </Paper>
                    </Grid>
                </Grid>
            </React.Fragment>
        );
    }
}

export default SignUpForm;