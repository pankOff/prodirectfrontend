import React from "react";
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import LogInForm from "./LogInForm";
import red from "@material-ui/core/colors/red";

class AddYandexUserForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {loaded: false, errorMessage: null, errorYandexReLogin: false};
    }

    onclick() {
        localStorage.setItem("current-yandex-user-name",document.getElementById("yandex-user-name").value);
        localStorage.setItem("current-yandex-user-email",document.getElementById("yandex-user-email").value);
        window.location.assign('https://oauth.yandex.ru/authorize?response_type=token&client_id=8332d031cef14743814e58aafe78e857');
    }

    addOneMore() {
        this.setState({loaded: false, errorMessage: null});
        window.location.assign(window.frontEndPoint+'/add_yandex_user/');
    }

    rollback() {
        window.location.assign(window.frontEndPoint+'/');
    }

    render() {
        if (localStorage.getItem("direct-auth-token")) {
            return (
                <React.Fragment>
                    {
                        !this.state.loaded
                        ? <Grid container style={{padding: 20, marginTop: 80}}
                                justify="center">
                                <Grid item xs={6}>
                                    <Paper>
                                        <Grid container
                                              direction="column"
                                              justify="center"
                                              alignItems="center"
                                              xs={12}>
                                            <Grid item xs={6} justify="center">
                                                {
                                                    this.state.errorYandexReLogin
                                                        ? <React.Fragment>
                                                        <h3>Такой Яндекс Пользователь уже зарегистрирован!</h3>
                                                            <p>Выдите выйдите из сервисов Яндекса и при повторной попытке зайдите под другим пользователем</p>
                                                        </React.Fragment>
                                                        : <h3>Добавьте нового <span style={{color: red}}>Яндекс Пользователя</span> чтобы управлять его кампаниями</h3>
                                                }
                                            </Grid>
                                        </Grid>
                                        <Grid container
                                              direction="row"
                                              justify="center"
                                              xs={12} style={{padding: 20}}
                                        >
                                            <Grid item xs={0}>
                                                <FormControl fullWidth={true}>
                                                    <div></div>
                                                    <div></div>
                                                    <div></div>
                                                </FormControl>
                                            </Grid>
                                            <Grid item xs={8}>
                                                <FormControl fullWidth={true}>
                                                    <TextField id="yandex-user-name" label="Имя Яндекс пользователя"/>
                                                    {
                                                        this.state.errorMessage != null
                                                        ? <TextField id="yandex-user-email"
                                                                     label="Email Яндекс пользователя"
                                                                     type="email"
                                                                     error
                                                                     helperText={this.state.errorMessage}
                                                            />
                                                        : <TextField id="yandex-user-email"
                                                                     label="Email Яндекс пользователя"
                                                                     type="email"/>
                                                    }
                                                </FormControl>
                                                <Button variant="contained" color="primary" style={{marginTop: 20, background: "#F6E332", color: "black"}}
                                                        onClick={(e) => this.onclick(e)}>
                                                    Зарегистрировать
                                                </Button>
                                            </Grid>
                                        </Grid>

                                    </Paper>
                                </Grid>
                            </Grid>
                            : <Grid container style={{padding: 20, marginTop: 80}}
                                    justify="center">
                                <Grid item xs={6}>
                                    <Paper>
                                        <Grid container
                                              direction="column"
                                              justify="center"
                                              alignItems="center"
                                              xs={12}>
                                            <Grid item xs={8} justify="center">
                                                <h2>Пользователь успешно добавлен!</h2>
                                            </Grid>
                                        </Grid>
                                        <Grid container
                                              direction="row"
                                              justify="center"
                                              xs={12} style={{padding: 20}}
                                        >
                                            <Grid item xs={1}>
                                                <FormControl fullWidth={true}>
                                                    <div></div>
                                                    <div></div>
                                                    <div></div>
                                                </FormControl>
                                            </Grid>
                                            <Grid item xs={8}>
                                                <Button variant="contained" style={{margin: 10, background: "#F6E332", color: "black"}}
                                                        onClick={(e) => this.addOneMore(e)}>
                                                    Добавить ещё
                                                </Button>
                                                <Button variant="contained" style={{margin: 20, background: "#F6E332", color: "black"}}
                                                        onClick={(e) => this.rollback(e)}>
                                                    Перейти на главную страницу
                                                </Button>
                                            </Grid>
                                        </Grid>

                                    </Paper>
                                </Grid>
                            </Grid>

                    }

                </React.Fragment>
            );
        } else {
            window.location.assign(window.frontEndPoint+'/login/');
            return <LogInForm/>;
        }
    }

    componentDidMount() {
        if (/access_token=([^&]+)/.exec(document.location.hash) != null) {
            this.saveYandexUser();
        }
    }

    saveYandexUser() {
        let formDate = {
            username: localStorage.getItem("current-yandex-user-name"),
            yandexAuthToken: /access_token=([^&]+)/.exec(document.location.hash)[1],
            email: localStorage.getItem("current-yandex-user-email")
        };
        /*this.setState({loaded: true})*/
        fetch(window.backEndPoint+'/addYandexUser', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem("direct-auth-token")
            },
            body: JSON.stringify(formDate)
        })
            .then((res) => res.json())
            .then((response) => {
                if (response.Status) {
                    this.setState({loaded:true})
                    localStorage.removeItem("current-yandex-user-name");
                    localStorage.removeItem("current-yandex-user-email");
                } else {
                    if (response.ErrorMessage === "Пользователь уже зарегистрирован") {
                        this.setState({errorYandexReLogin: true})
                    } else {
                        this.setState({errorMessage: response.ErrorMessage})
                    }
                }
            });
    }
}

export default AddYandexUserForm;