import React from "react";
import Grid from "@material-ui/core/Grid";
import ListItemsTmp from "./ListItems";
import {fetchUsersAndCampaigns} from "../store/actions";
import {connect} from "react-redux";
import LinearProgress from "@material-ui/core/LinearProgress";
import CircularProgress from '@material-ui/core/CircularProgress';
import { AdGroupFieldConnected } from "./AdGroupField";
import Paper from "@material-ui/core/Paper";
import {StrategySettingsConnected} from "./StrategySettings";
import LogInForm from "./login/LogInForm";
import Container from "@material-ui/core/Container";

class RootElement extends React.Component{
    constructor(props) {
        super(props);
        this.setSelectedUser = this.setSelectedUser.bind(this);
        this.setSelectedCampaign = this.setSelectedCampaign.bind(this);
        this.selectCampaignForStrategy = this.selectCampaignForStrategy.bind(this);
        this.selectAdGroupForStrategy = this.selectAdGroupForStrategy.bind(this);
        this.selectKeywordForStrategy = this.selectKeywordForStrategy.bind(this);
        this.state = {selectedUser : '',
            selectedCampaign : '',
            adGroupsPending : false,
            campaignsForStrategy : [],
            adGroupsForStrategy : [],
            keywordsForStrategy : []
        };
    }

    setSelectedUser(userEmail) {
        this.setState({selectedUser : userEmail});
        this.setState({adGroupsPending : true});
    }

    setSelectedCampaign(id) {
        this.setState({selectedCampaign : id});
        this.setState({adGroupsPending : false});
    }

    selectCampaignForStrategy(id) {
        let campaignsForStrategy = this.state.campaignsForStrategy;
        if (campaignsForStrategy.includes(id)) {
            const index = campaignsForStrategy.indexOf(id);
            if (index > -1) {
                campaignsForStrategy.splice(index, 1);
            }
        } else {
            campaignsForStrategy.push(id)
        }
        this.setState({campaignsForStrategy : campaignsForStrategy});
        console.log(this.state.campaignsForStrategy)
    }

    selectAdGroupForStrategy(id) {
        let adGroupsForStrategy = this.state.adGroupsForStrategy;
        if (adGroupsForStrategy.includes(id)) {
            const index = adGroupsForStrategy.indexOf(id);
            if (index > -1) {
                adGroupsForStrategy.splice(index, 1);
            }
        } else {
            adGroupsForStrategy.push(id)
        }
        this.setState({adGroupsForStrategy : adGroupsForStrategy});
        console.log(this.state.adGroupsForStrategy)
    }

    selectKeywordsForStrategy(ids) {
        let keywordsForStrategy = this.state.keywordsForStrategy;
        let tmp = keywordsForStrategy.concat(ids);
        keywordsForStrategy = tmp.filter(function (item, pos) {return tmp.indexOf(item) == pos});


    }
    selectKeywordForStrategy(id) {
        let keywordsForStrategy = this.state.keywordsForStrategy;
        if (keywordsForStrategy.includes(id)) {
            const index = keywordsForStrategy.indexOf(id);
            if (index > -1) {
                keywordsForStrategy.splice(index, 1);
            }
        } else {
            keywordsForStrategy.push(id)
        }
        this.setState({keywordsForStrategy : keywordsForStrategy});
    }

    render() {
        if (localStorage.getItem("direct-auth-token")) {
        const { users = [], pending = true} = this.props;
        return (
            <React.Fragment>
                <Grid container>
                    <Container maxWidth="sm">
                        <Paper style={{padding: 20}}>
                            <StrategySettingsConnected
                                campaignsForStrategy={this.state.campaignsForStrategy}
                                adGroupsForStrategy={this.state.adGroupsForStrategy}
                                keywordsForStrategy={this.state.keywordsForStrategy}
                                selectedUser={this.state.selectedUser}
                                isMultiple={true}
                            />
                        </Paper>
                    </Container>
                </Grid>
                {
                    pending
                        ? <LinearProgress/>
                        : <Grid container spacing={1} container style={{ padding: 20 }}>

                            <Grid item xs={1}>
                                <ListItemsTmp name={'Аккаунты'}
                                              onChange={this.setSelectedUser}
                                              selectedUser={this.state.selectedUser}
                                              selectedCampaign={this.state.selectedCampaign}
                                              users={users}
                                />
                            </Grid>
                            <Grid item xs={2}>
                                <ListItemsTmp name={'Кампании'}
                                              onChange={this.setSelectedCampaign}
                                              selectedUser={this.state.selectedUser}
                                              selectedCampaign={this.state.selectedCampaign}
                                              onClickCheckbox={this.selectCampaignForStrategy}
                                              users={users}
                                />
                            </Grid>
                            {
                                this.state.adGroupsPending
                                    ? <React.Fragment></React.Fragment>
                                    : <AdGroupFieldConnected selectedCampaign={this.state.selectedCampaign}
                                                             selectedUser={this.state.selectedUser}
                                                             onClickCheckbox={this.selectAdGroupForStrategy}
                                                             onClickKeywordCheckbox={this.selectKeywordForStrategy}
                                                             selectedKeywords={this.state.keywordsForStrategy}
                                    />
                            }

                        </Grid>
                }

            </React.Fragment>
        );
        } else {
            window.location.assign(window.frontEndPoint+'/login/');
            return <LogInForm/>;
        }
    }

    componentDidMount() {
        this.props.fetchUsersAndCampaigns();
    }

}

const mapStateToProps = ({ users, usersPending }) => ({
    users,
    pending: usersPending,
});

const mapDispatchToProps = (dispatch) => ({
    fetchUsersAndCampaigns: () => dispatch(fetchUsersAndCampaigns())
});

export const RootElementConnected = connect(mapStateToProps, mapDispatchToProps)(RootElement);
