import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import Input from "@material-ui/core/Input";
import MenuItem from "@material-ui/core/MenuItem";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import React from "react";
import {fetchStrategies, putAdGroups} from "../store/actions";
import connect from "react-redux/es/connect/connect";
import Button from "@material-ui/core/Button";
import SaveIcon from "@material-ui/icons/Save";
import Tooltip from "@material-ui/core/Tooltip";

const SELECT_DESCRIPTIONS = {
    "mainStrategy": "Основная стратегия",
    "extraStrategy": "Дополнительная стратегия",
};

class StrategySettings extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.setStrategyForSelected = this.setStrategyForSelected.bind(this);
        this.state = {mainStrategy : null, extraStrategy : null, maxBidMain : null, maxBidExtra : null};
    }

    renderSelect (name, menuItems) {
        return <Grid item xs={8}>
            <FormControl fullWidth={true}>
                <InputLabel shrink={Boolean(this.state[name])} htmlFor={name}>
                     {SELECT_DESCRIPTIONS[name]}
                </InputLabel>
                {this.props.isMultiple ?
                <Select input={<Input name={name} id={name}/>}
                        onChange={event => this.handleChange(event)}
                        value={this.state[name]}
                >
                    { menuItems }
                </Select>
                    :
                    <Select input={<Input name={name} id={name}/>}
                            onChange={event => this.handleChange(event)}
                    >
                        { menuItems }
                    </Select>
                }
            </FormControl>
        </Grid>
    }

    renderTextField (name) {
        return <Grid item xs={4}>
            <TextField
                required
                style={{ marginTop: 0}}
                name={ name }
                label="Макс. ставка"
                margin="normal"
                fullWidth={true}
                onChange={event => this.handleMaxBidChange(event)}
            />
        </Grid>;
    }

    showKeywordInYandex() {
        window.open("http://yandex.ru/yandsearch?text="+this.props.keyword+"&amp;lr=1");
    }

    setStrategyForSelected() {
        let yandexUser = {email:this.props.selectedUser}
        let formDate = {
            yandexUser: yandexUser,
            adGroups:[],//this.props.adGroupsForStrategy,
            campaigns:[],//this.props.campaignsForStrategy,
            keywords:this.props.keywordsForStrategy,
            mainStrategyMaxBid:this.state.maxBidMain,
            secondStrategyMaxBid:this.state.maxBidExtra,
            mainStrategy:{
                name:this.state.mainStrategy
            },
            secondStrategy:{
                name:this.state.extraStrategy
            }
        };
        console.log("form ", formDate);
        fetch(window.backEndPoint+'/setStrategy', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem("direct-auth-token")
            },
            body: JSON.stringify(formDate)
        })
            .then((res) => res.json())
            .then((response) => {
                if (!response.Status) {
                    console.log("ERROR")
                } else {
                    alert("Стратегия установлена")
                    window.location.assign(window.frontEndPoint+'/');
                }
            });
    }

    render() {
        const menuItems = this.props.strategies.map(({ name, formula }) =>
            <MenuItem value={ name } key={ name }>
                { name }
            </MenuItem>
        );
        return (
            <React.Fragment>
                <Grid container spacing={4}>
                    { this.renderSelect("mainStrategy", menuItems) }
                    { this.renderTextField("max-bid-main") }
                </Grid>
                <Grid container spacing={4}>
                    { this.renderSelect("extraStrategy", menuItems) }
                    { this.renderTextField("max-bid-extra") }
                </Grid>
                {this.props.isMultiple ?
                <Grid container spacing={8}>
                    <Grid item xs={12}>
                        <Button fullWidth={false}
                                variant="contained"
                                style = {{float: "right", marginTop: 10}}
                                onClick={event => this.setStrategyForSelected(event)}
                        >
                            Применить к выделенным
                        </Button>
                    </Grid>
                </Grid>
                :
                    <React.Fragment>
                    <Tooltip title="Фраза на поиске" placement="right">
                        <Button size="small"
                            startIcon={<img src={"Yandex-icon.webp"} width={"20"} height={"20"}/>}
                            style = {{float: "left"}}
                            onClick={(event) => this.showKeywordInYandex(event)}>
                        </Button>
                    </Tooltip>
                    <Button size="small"
                            startIcon={<SaveIcon />}
                            style = {{float: "right"}}
                            onClick={(event) => this.setStrategyForSelected(event)}>
                        Save
                    </Button>
                    </React.Fragment>
                }
            </React.Fragment>
        )
    }

    componentDidMount() {
        this.props.fetchStrategies();
    }

    handleMaxBidChange(event) {
        if (event.target.name === "max-bid-main") {
            console.log(event.target.value)
            this.setState({ maxBidMain : event.target.value*1000000 });
        }
        if (event.target.name === "max-bid-extra") {
            this.setState({ maxBidExtra : event.target.value*1000000 });
        }
    }

    handleChange(event) {
        if (event.target.name === "mainStrategy") {
            console.log(event.target.value)
            this.setState({ mainStrategy : event.target.value });
        }
        if (event.target.name === "extraStrategy") {
            this.setState({ extraStrategy : event.target.value });
        }

        console.log(this.state)
    };
}

const mapStateToProps = ({ strategies = [] }) => ({
    strategies,
});

const mapDispatchToProps = (dispatch) => ({
    fetchStrategies: () => dispatch(fetchStrategies()),
});

export const StrategySettingsConnected = connect(mapStateToProps, mapDispatchToProps)(StrategySettings);
