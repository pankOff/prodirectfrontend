
import React from "react";
import {fetchStrategies, putAdGroups} from "../store/actions";
import connect from "react-redux/es/connect/connect";
import Container from "@material-ui/core/Container";
import LogInForm from "./login/LogInForm";
import StrategiesTable from "./StrategiesTable";
import AssignmentIcon from '@material-ui/icons/Assignment';
import Button from "@material-ui/core/Button";

class Strategies extends React.Component {
    constructor(props) {
        super(props);
    }


    render() {

        if (localStorage.getItem("direct-auth-token")) {
            return (
                <React.Fragment>
                    <Container maxWidth="lg" style={{paddingTop: 20}}>
                        <Button endIcon={<AssignmentIcon/>}
                                style = {{float: "right"}}
                                variant="outlined"
                                size="large"
                                onClick={function () {document.location.href = window.frontEndPoint+"/new_strategy"}}
                        >
                            Добавить новую стратегию
                        </Button>
                        <StrategiesTable
                            strategyList={this.props.strategies}
                        />
                    </Container>
                </React.Fragment>
            );
        } else {
            window.location.assign(window.frontEndPoint+'/login/');
            return <LogInForm/>;
        }
    }

    componentDidMount() {
        this.props.fetchStrategies();
    }

}

const mapStateToProps = ({ strategies = [] }) => ({
    strategies,
});

const mapDispatchToProps = (dispatch) => ({
    fetchStrategies: () => dispatch(fetchStrategies()),
});

export const StrategiesTestConnected = connect(mapStateToProps, mapDispatchToProps)(Strategies);
