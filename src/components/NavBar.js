import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import connect from "react-redux/es/connect/connect";
import DirectMenu from "./DirectMenu";
import LoginMenu from "./login/LoginMenu";

function jivoOpen () {
    try {
        window.jivo_api.open();
    } catch (e) {
        //
    }
};

function signout() {
    localStorage.removeItem("direct-auth-token");
    window.location.assign(window.frontEndPoint+'/login/');
}

/*const PhoneNumber = (jivoLoaded) => {
    return <span style={{ display: 'inline-block', fontSize: '15px', width: '50%', maxWidth: '325px', textAlign: 'center' }} >
        <div style={{ marginBottom: '5px' }}>+7 (986) 786-85-43</div>
        { jivoLoaded && <Fab variant="extended"
                             size="small"
                             onClick={jivoOpen}
                             style={{ height: '20px', background: '#F6E332', textTransform: 'none' }}>
            Заказать обратный звонок
        </Fab> }
    </span>
};*/
let anchorEl = null;

const handleClick = (event) => {
    anchorEl = event.currentTarget;
};

const handleClose = () => {
    anchorEl = null;
};


const NavBar = ({ jivoLoaded }) => {
    return(
        <div>
            <AppBar position="static" style={{ background: '#202020' }}>
                <Toolbar style = {{color: "white"}}>
                    <DirectMenu/>
                    <LoginMenu/>
                    {/*<PhoneNumber jivoLoaded={jivoLoaded}/>*/}
                </Toolbar>
            </AppBar>
        </div>
    )
};

const mapStateToProps = ({ jivoLoaded = [] }) => ({
    jivoLoaded
});

export const NavBarConntected = connect(mapStateToProps)(NavBar);
