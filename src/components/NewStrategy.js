import React from "react";
import Container from '@material-ui/core/Container';
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputAdornment from "@material-ui/core/InputAdornment";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import SaveIcon from "@material-ui/icons/Save";
import VariablesTable from "./VariablesTable";
import {putAdGroups} from "../store/actions";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";


class NewStrategy extends React.Component{

    saveStrategy() {
        let formDate = {
            name: document.getElementById("strategy-name").value,
            formula: document.getElementById("strategy-formula").value
        };

        fetch(window.backEndPoint+'/strategy/create', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem("direct-auth-token")
            },
            body: JSON.stringify(formDate)
        }).then((res) => res)
            .then((data) => {
                window.location.assign(window.frontEndPoint+'/strategies');
            });
    }


    render() {
        return (
            <React.Fragment>
                <Container maxWidth="sm" style={{paddingTop: 20}}>
                    <Grid container
                          direction="row"
                          justify="flex-start"
                          alignItems="center"
                          style={{marginBottom: 20}}
                          spacing={3}>
                        <Grid item xs>
                            <TextField id="strategy-name" fullWidth label="Название"/>
                        </Grid>
                    </Grid>
                    <Grid container
                          direction="row"
                          justify="flex-start"
                          alignItems="center"
                          style={{marginBottom: 20}}
                          spacing={3}>
                        <Grid item xs>
                            <FormControl fullWidth variant="outlined">
                                <InputLabel htmlFor="outlined-adornment-amount">Стратегия</InputLabel>
                                <OutlinedInput
                                    id={"strategy-formula"}
                                    startAdornment={<InputAdornment position="start"></InputAdornment>}
                                    multiline
                                    rows={8}
                                    labelWidth={80}
                                />
                            </FormControl>
                        </Grid>
                    </Grid>
                    <Grid
                        container
                        direction="row"
                        justify="flex-end"
                        alignItems="center"
                    >
                        <Button startIcon={<SaveIcon />}
                                style = {{float: "right"}}
                                variant="outlined"
                                size="large"
                                onClick={() => this.saveStrategy()}
                        >
                            Save
                        </Button>
                    </Grid>
                </Container>
                <Container maxWidth="sm" style={{paddingTop: 10}}>
                    <Typography variant="h6" id="exampleTitle" component="div">
                        Условный оператор. Пример
                    </Typography>
                    <Paper style={{padding: 20}}>
                        <Typography>
                            {"   if ( (1СЦ -2СЦ) /2СЦ < 0.15 && (1СЦ -3СЦ) /3СЦ < 0.30 ) {"}
                        </Typography>
                        <Typography>
                            {"      1С + 0.1"}
                        </Typography>
                        <Typography>
                            {"   } else if ( (2СЦ -3СЦ) /3СЦ < 0.15) {"}
                        </Typography>
                        <Typography>
                            {"      2С + 0.1\n"}
                        </Typography>
                        <Typography>
                            {"   } else {\n"}
                        </Typography>
                        <Typography>
                            {"      3С + 0.1\n"}
                        </Typography>
                        <Typography>
                            {"   }"}
                        </Typography>
                    </Paper>
                </Container>
                <Container maxWidth="sm" style={{paddingTop: 10}}>
                    <VariablesTable/>
                </Container>
            </React.Fragment>
        );
    }
}

export default NewStrategy;