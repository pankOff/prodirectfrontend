import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { Link } from "react-router-dom";
import LinearProgress from '@material-ui/core/LinearProgress';
import { connect } from 'react-redux'
import React from "react";
import { fetchCampaings, selectAllCampaigns, selectCampaign } from "../store/actions";
import MUIDataTable from "mui-datatables";
import * as textLabels from "../mui-datatables-textlabels-ru";

const CAMPAIGN_STATUS = {
    ON: 'Кампания активна',
    SUSPENDED: 'Кампания неактивна',
    OFF: 'Кампания отключена',
    ENDED: 'Кампания завершена',
    ARCHIVED: 'Кампания в архиве',
    CONVERTED: 'Кампания в архиве',
};

const columns = [
    {
        label: "№",
        options: {
            filter: false,
            sort: true,
            customBodyRender: (value, { rowData }) => {
                return <Link to={`/campaign?id=${rowData[0]}`}>{ '№' + value }</Link>
            }
        }
    },
    {
        label: "Кампания",
        options: {
            filter: false,
            sort: true,
            customBodyRender: (value, { rowData }) => {
                return <Link to={`/campaign?id=${rowData[0]}`}>{ value }</Link>
            }
        }
    },
    {
        label: "Логин",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        label: "Состояние",
        options: {
            filter: true,
            sort: true,
        }
    },
];

const DEFAULT_OPTIONS = {
    filterType: 'checkbox',
    viewColumns: false,
    download: false,
    print: false,
    textLabels,
    customToolbarSelect: () => <React.Fragment/>,
};

class CampaignsList extends React.Component {
    render() {
        const { campaigns = [], pending = true, onSelectCampaign, onSelectAll } = this.props;

        const tableData = campaigns.map((campaign) => {
            return [
                campaign.Id,
                campaign.Name,
                campaign.ClientInfo,
                CAMPAIGN_STATUS[campaign.State],
            ];
        });

        const options = {
            ...DEFAULT_OPTIONS,
            onRowsSelect: (currentRowsSelected, allRowsSelected) => {
                const selectedIds = allRowsSelected.map(({ dataIndex }) => campaigns[dataIndex].Id);
                console.log({ selectedIds });
            },
        };

        return <Grid container style={{ padding: 20 }}>
            <Grid item xs={12}>
                <Paper>
                    {
                        pending
                            ? <LinearProgress/>
                            : <MUIDataTable
                                data={tableData}
                                columns={columns}
                                options={options}
                            />
                    }
                </Paper>
            </Grid>
        </Grid>;
    }

    componentDidMount() {
        this.props.fetchCampaigns();
    }
}

const mapStateToProps = ({ campaigns, campaignsPending }) => ({
    campaigns,
    pending: campaignsPending,
});

const mapDispatchToProps = (dispatch) => ({
    fetchCampaigns: () => dispatch(fetchCampaings()),
    onSelectCampaign: (campaignId, selected) => dispatch(selectCampaign({ campaignId, selected })),
    onSelectAll: (selected) => dispatch(selectAllCampaigns({ selected })),
});

export const CampaignsListConnected = connect(mapStateToProps, mapDispatchToProps)(CampaignsList);
